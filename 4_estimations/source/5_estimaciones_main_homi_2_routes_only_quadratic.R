#======================================#
# Elaborado por: Eduard Martinez
# Ultima modificacion: 19/11/2021 
# R version 4.1.1 (2021-08-10) 
#======================================#

# initial configuration
rm(list = ls())
require(pacman)
p_load(rio,tidyverse,fixest,lfe,plm,lmtest,stargazer,broom)

# config
Sys.setlocale("LC_CTYPE", "en_US.UTF-8")

#=====================#
# Number of homicides #
#=====================#

# import data
db = import("4_estimations/output/data_mpo_year-month_2016_2020.rds") %>% subset(d_2016_2020==1) 

# models
modelo_q_ruta = '~ retor_m + q_10_2ruta + q_10_2ruta_ret | mpio_code + year_month | 0 | mpio_code'
modelo_n_ruta = '~ retor_m + n_10_2ruta + n_10_2ruta_ret | mpio_code + year_month | 0 | mpio_code'

# results
homi_q_ruta = felm(as.formula(paste0("homicidios",modelo_q_ruta)) , data=db , subset=d_2016_2020==1) 
homi_n_ruta = felm(as.formula(paste0("homicidios",modelo_n_ruta)) , data=db , subset=d_2016_2020==1) 

db = db %>% 
     select(d_2016_2020,homicidios,retor_m,mpio_code,year_month,
            q_10_ruta2,n_10_ruta2,q_10_ruta2_ret,n_10_ruta2_ret,
            q_10_2ruta2,n_10_2ruta2,q_10_2ruta2_ret,n_10_2ruta2_ret) %>% 
        rename(q_10_ruta=q_10_ruta2 , n_10_ruta=n_10_ruta2 , q_10_ruta_ret=q_10_ruta2_ret , n_10_ruta_ret=n_10_ruta2_ret,
               q_10_2ruta=q_10_2ruta2 , n_10_2ruta=n_10_2ruta2 , q_10_2ruta_ret=q_10_2ruta2_ret , n_10_2ruta_ret=n_10_2ruta2_ret)

homi_q_ruta2 = felm(as.formula(paste0("homicidios",modelo_q_ruta)) , data=db , subset=d_2016_2020==1) 
homi_n_ruta2 = felm(as.formula(paste0("homicidios",modelo_n_ruta)) , data=db , subset=d_2016_2020==1) 

# Tabla prinicpal
labels_dependientes = c('Returnees',
                        'f(Dist. route)*Seizures (Q)','Retur*f(Dist. route)*Seizures (Q)',
                        'f(Dist. route)*Seizures (N)','Retur*f(Dist. route)*Seizures (N)')

stargazer(homi_q_ruta, homi_n_ruta, homi_q_ruta2, homi_n_ruta2,
          header=F, type= 'text',
          dep.var.labels.include = F,
          label = 'q',
          title = 'Effect of drugs scarcity on homicides',
          column.labels = c('UNODC', 'Insight crime'),
          column.separate = c(2, 2),
          covariate.labels = labels_dependientes, 
          table.placement = 'H', 
          df = FALSE,
          digits = 4, 
          omit.stat = c('f', 'ser', 'rsq', 'adj.rsq'),
          add.lines = list(c('Time frame', '2016-2020', '2016-2020', '2016-2020', '2016-2020'),
                           c('F.E. Municipality', 'Yes', 'Yes', 'Yes', 'Yes'),
                           c('F.E. Year-Month', 'Yes', 'Yes', 'Yes', 'Yes')),
          notes.align = "l", notes.append = T,
          out = '6_informe/input/tables/reghdfe_2routes_only_quadratic_abs_homi.tex')


#============================#
# Homicides ratio by 100.000 #
#============================#

# clean environment
rm(list=ls())

# import data
db = import("4_estimations/output/data_mpo_year-month_2016_2020.rds") %>% subset(d_2016_2020==1) 

# models
modelo_q_ruta = '~ retor_m + q_10_2ruta + q_10_2ruta_ret | mpio_code + year_month | 0 | mpio_code'
modelo_n_ruta = '~ retor_m + n_10_2ruta + n_10_2ruta_ret | mpio_code + year_month | 0 | mpio_code'

# results
homi_q_ruta = felm(as.formula(paste0("ratio_homicidios",modelo_q_ruta)) , data=db , subset=d_2016_2020==1) 
homi_n_ruta = felm(as.formula(paste0("ratio_homicidios",modelo_n_ruta)) , data=db , subset=d_2016_2020==1) 

db = db %>% 
        select(d_2016_2020,ratio_homicidios,retor_m,mpio_code,year_month,
               q_10_ruta2,n_10_ruta2,q_10_ruta2_ret,n_10_ruta2_ret,
               q_10_2ruta2,n_10_2ruta2,q_10_2ruta2_ret,n_10_2ruta2_ret) %>% 
        rename(q_10_ruta=q_10_ruta2 , n_10_ruta=n_10_ruta2 , q_10_ruta_ret=q_10_ruta2_ret , n_10_ruta_ret=n_10_ruta2_ret,
               q_10_2ruta=q_10_2ruta2 , n_10_2ruta=n_10_2ruta2 , q_10_2ruta_ret=q_10_2ruta2_ret , n_10_2ruta_ret=n_10_2ruta2_ret)

homi_q_ruta2 = felm(as.formula(paste0("ratio_homicidios",modelo_q_ruta)) , data=db , subset=d_2016_2020==1) 
homi_n_ruta2 = felm(as.formula(paste0("ratio_homicidios",modelo_n_ruta)) , data=db , subset=d_2016_2020==1) 

# Tabla prinicpal
labels_dependientes = c('Returnees',
                        'f(Dist. route)*Seizures (Q)','Retur*f(Dist. route)*Seizures (Q)',
                        'f(Dist. route)*Seizures (N)','Retur*f(Dist. route)*Seizures (N)')

stargazer(homi_q_ruta, homi_n_ruta, homi_q_ruta2, homi_n_ruta2,
          header=F, type= 'text',
          dep.var.labels.include = F,
          label = 'q',
          title = 'Effect of drugs scarcity on homicides',
          column.labels = c('UNODC', 'Insight crime'),
          column.separate = c(2, 2),
          covariate.labels = labels_dependientes, 
          table.placement = 'H', 
          df = FALSE,
          digits = 4, 
          omit.stat = c('f', 'ser', 'rsq', 'adj.rsq'),
          add.lines = list(c('Time frame', '2016-2020', '2016-2020', '2016-2020', '2016-2020'),
                           c('F.E. Municipality', 'Yes', 'Yes', 'Yes', 'Yes'),
                           c('F.E. Year-Month', 'Yes', 'Yes', 'Yes', 'Yes')),
          notes.align = "l", notes.append = T,
          out = '6_informe/input/tables/reghdfe_2routes_only_quadratic_ratio_homi.tex')

#===============#
# Log homicides #
#===============#

# clean environment
rm(list=ls())

# import data
db = import("4_estimations/output/data_mpo_year-month_2016_2020.rds") %>% subset(d_2016_2020==1) 

# models
modelo_q_ruta = '~ retor_m + q_10_2ruta + q_10_2ruta_ret | mpio_code + year_month | 0 | mpio_code'
modelo_n_ruta = '~ retor_m + n_10_2ruta + n_10_2ruta_ret | mpio_code + year_month | 0 | mpio_code'

# results
homi_q_ruta = felm(as.formula(paste0("ln_homicidios",modelo_q_ruta)) , data=db , subset=d_2016_2020==1) 
homi_n_ruta = felm(as.formula(paste0("ln_homicidios",modelo_n_ruta)) , data=db , subset=d_2016_2020==1) 

db = db %>% 
        select(d_2016_2020,ln_homicidios,retor_m,mpio_code,year_month,
               q_10_ruta2,n_10_ruta2,q_10_ruta2_ret,n_10_ruta2_ret,
               q_10_2ruta2,n_10_2ruta2,q_10_2ruta2_ret,n_10_2ruta2_ret) %>% 
        rename(q_10_ruta=q_10_ruta2 , n_10_ruta=n_10_ruta2 , q_10_ruta_ret=q_10_ruta2_ret , n_10_ruta_ret=n_10_ruta2_ret,
               q_10_2ruta=q_10_2ruta2 , n_10_2ruta=n_10_2ruta2 , q_10_2ruta_ret=q_10_2ruta2_ret , n_10_2ruta_ret=n_10_2ruta2_ret)

homi_q_ruta2 = felm(as.formula(paste0("ln_homicidios",modelo_q_ruta)) , data=db , subset=d_2016_2020==1) 
homi_n_ruta2 = felm(as.formula(paste0("ln_homicidios",modelo_n_ruta)) , data=db , subset=d_2016_2020==1) 

# Tabla prinicpal
labels_dependientes = c('Returnees',
                        'f(Dist. route)*Seizures (Q)','Retur*f(Dist. route)*Seizures (Q)',
                        'f(Dist. route)*Seizures (N)','Retur*f(Dist. route)*Seizures (N)')

stargazer(homi_q_ruta, homi_n_ruta, homi_q_ruta2, homi_n_ruta2,
          header=F, type= 'text',
          dep.var.labels.include = F,
          label = 'q',
          title = 'Effect of drugs scarcity on homicides',
          column.labels = c('UNODC', 'Insight crime'),
          column.separate = c(2, 2),
          covariate.labels = labels_dependientes, 
          table.placement = 'H', 
          df = FALSE,
          digits = 4, 
          omit.stat = c('f', 'ser', 'rsq', 'adj.rsq'),
          add.lines = list(c('Time frame', '2016-2020', '2016-2020', '2016-2020', '2016-2020'),
                           c('F.E. Municipality', 'Yes', 'Yes', 'Yes', 'Yes'),
                           c('F.E. Year-Month', 'Yes', 'Yes', 'Yes', 'Yes')),
          notes.align = "l", notes.append = T,
          out = '6_informe/input/tables/reghdfe_2routes_ln_only_quadratic_homi.tex')

#=====================#
# Log ratio-homicides #
#=====================#

# clean environment
rm(list=ls())

# import data
db = import("4_estimations/output/data_mpo_year-month_2016_2020.rds") %>% subset(d_2016_2020==1) 

# models
modelo_q_ruta = '~ retor_m + q_10_2ruta + q_10_2ruta_ret | mpio_code + year_month | 0 | mpio_code'
modelo_n_ruta = '~ retor_m + n_10_2ruta + n_10_2ruta_ret | mpio_code + year_month | 0 | mpio_code'

# results
homi_q_ruta = felm(as.formula(paste0("ln_ratio_homicidios",modelo_q_ruta)) , data=db , subset=d_2016_2020==1) 
homi_n_ruta = felm(as.formula(paste0("ln_ratio_homicidios",modelo_n_ruta)) , data=db , subset=d_2016_2020==1) 

db = db %>% 
        select(d_2016_2020,ln_ratio_homicidios,retor_m,mpio_code,year_month,
               q_10_ruta2,n_10_ruta2,q_10_ruta2_ret,n_10_ruta2_ret,
               q_10_2ruta2,n_10_2ruta2,q_10_2ruta2_ret,n_10_2ruta2_ret) %>% 
        rename(q_10_ruta=q_10_ruta2 , n_10_ruta=n_10_ruta2 , q_10_ruta_ret=q_10_ruta2_ret , n_10_ruta_ret=n_10_ruta2_ret,
               q_10_2ruta=q_10_2ruta2 , n_10_2ruta=n_10_2ruta2 , q_10_2ruta_ret=q_10_2ruta2_ret , n_10_2ruta_ret=n_10_2ruta2_ret)

homi_q_ruta2 = felm(as.formula(paste0("ln_ratio_homicidios",modelo_q_ruta)) , data=db , subset=d_2016_2020==1) 
homi_n_ruta2 = felm(as.formula(paste0("ln_ratio_homicidios",modelo_n_ruta)) , data=db , subset=d_2016_2020==1) 

# Tabla prinicpal
labels_dependientes = c('Returnees',
                        'f(Dist. route)*Seizures (Q)','Retur*f(Dist. route)*Seizures (Q)',
                        'f(Dist. route)*Seizures (N)','Retur*f(Dist. route)*Seizures (N)')

stargazer(homi_q_ruta, homi_n_ruta, homi_q_ruta2, homi_n_ruta2,
          header=F, type= 'text',
          dep.var.labels.include = F,
          label = 'q',
          title = 'Effect of drugs scarcity on homicides',
          column.labels = c('UNODC', 'Insight crime'),
          column.separate = c(2, 2),
          covariate.labels = labels_dependientes, 
          table.placement = 'H', 
          df = FALSE,
          digits = 4, 
          omit.stat = c('f', 'ser', 'rsq', 'adj.rsq'),
          add.lines = list(c('Time frame', '2016-2020', '2016-2020', '2016-2020', '2016-2020'),
                           c('F.E. Municipality', 'Yes', 'Yes', 'Yes', 'Yes'),
                           c('F.E. Year-Month', 'Yes', 'Yes', 'Yes', 'Yes')),
          notes.align = "l", notes.append = T,
          out = '6_informe/input/tables/reghdfe_2routes_ln_ratio_only_quadratic_homi.tex')



