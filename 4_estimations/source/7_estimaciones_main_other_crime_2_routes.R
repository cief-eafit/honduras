#======================================#
# Elaborado por: Eduard Martinez
# Ultima modificacion: 19/11/2021 
# R version 4.1.1 (2021-08-10) 
#======================================#

# initial configuration
rm(list = ls())
require(pacman)
p_load(rio,tidyverse,fixest,lfe,plm,lmtest,stargazer,broom)

# config
Sys.setlocale("LC_CTYPE", "en_US.UTF-8")

#=====================#
# Number of homicides #
#=====================#

# import data
db = import("4_estimations/output/data_mpo_year-month_2016_2020.rds") %>% subset(d_2016_2020==1) 

# models
modelo_q_ruta = '~ retor_m + q_10_ruta + q_10_ruta_ret | mpio_code + year_month | 0 | mpio_code'
modelo_n_ruta = '~ retor_m + n_10_ruta + n_10_ruta_ret | mpio_code + year_month | 0 | mpio_code'

# results
homi_q_ruta = felm(as.formula(paste0("propiedad",modelo_q_ruta)) , data=db , subset=d_2016_2020==1) 
homi_n_ruta = felm(as.formula(paste0("propiedad",modelo_n_ruta)) , data=db , subset=d_2016_2020==1) 

db = db %>% 
     select(d_2016_2020,propiedad,retor_m,mpio_code,year_month,q_10_ruta2,n_10_ruta2,q_10_ruta2_ret,n_10_ruta2_ret) %>% 
     rename(q_10_ruta=q_10_ruta2 , n_10_ruta=n_10_ruta2 , q_10_ruta_ret=q_10_ruta2_ret , n_10_ruta_ret=n_10_ruta2_ret)

homi_q_ruta2 = felm(as.formula(paste0("propiedad",modelo_q_ruta)) , data=db , subset=d_2016_2020==1) 
homi_n_ruta2 = felm(as.formula(paste0("propiedad",modelo_n_ruta)) , data=db , subset=d_2016_2020==1) 

# Tabla prinicpal
labels_dependientes = c('Returnees',
                        'Dist. route*Seizures (Q)','Retur*Dist. route*Seizures (Q)',
                        'Dist. route*Seizures (N)','Retur*Dist. route*Seizures (N)')

stargazer(homi_q_ruta, homi_n_ruta, homi_q_ruta2, homi_n_ruta2,
          header=F, type= 'text',
          dep.var.labels.include = F,
          label = 'q',
          title = 'Effect of drugs scarcity on property crimes',
          column.labels = c('UNODC', 'Insight crime'),
          column.separate = c(2, 2),
          covariate.labels = labels_dependientes, 
          table.placement = 'H', 
          df = FALSE,
          digits = 4, 
          omit.stat = c('f', 'ser', 'rsq', 'adj.rsq'),
          add.lines = list(c('Time frame', '2016-2020', '2016-2020', '2016-2020', '2016-2020'),
                           c('F.E. Municipality', 'Yes', 'Yes', 'Yes', 'Yes'),
                           c('F.E. Year-Month', 'Yes', 'Yes', 'Yes', 'Yes')),
          notes.align = "l", notes.append = T,
          out = '6_informe/input/tables/reghdfe_2routes_abs_property.tex')


#============================#
# Homicides ratio by 100.000 #
#============================#

# clean environment
rm(list=ls())

# import data
db = import("4_estimations/output/data_mpo_year-month_2016_2020.rds") %>% subset(d_2016_2020==1) 

# models
modelo_q_ruta = '~ retor_m + q_10_ruta + q_10_ruta_ret | mpio_code + year_month | 0 | mpio_code'
modelo_n_ruta = '~ retor_m + n_10_ruta + n_10_ruta_ret | mpio_code + year_month | 0 | mpio_code'

# results
homi_q_ruta = felm(as.formula(paste0("ratio_propiedad",modelo_q_ruta)) , data=db , subset=d_2016_2020==1) 
homi_n_ruta = felm(as.formula(paste0("ratio_propiedad",modelo_n_ruta)) , data=db , subset=d_2016_2020==1) 

db = db %>% 
     select(d_2016_2020,ratio_propiedad,retor_m,mpio_code,year_month,q_10_ruta2,n_10_ruta2,q_10_ruta2_ret,n_10_ruta2_ret) %>% 
     rename(q_10_ruta=q_10_ruta2 , n_10_ruta=n_10_ruta2 , q_10_ruta_ret=q_10_ruta2_ret , n_10_ruta_ret=n_10_ruta2_ret)

homi_q_ruta2 = felm(as.formula(paste0("ratio_propiedad",modelo_q_ruta)) , data=db , subset=d_2016_2020==1) 
homi_n_ruta2 = felm(as.formula(paste0("ratio_propiedad",modelo_n_ruta)) , data=db , subset=d_2016_2020==1) 

# Tabla prinicpal
labels_dependientes = c('Returnees',
                        'Dist. route*Seizures (Q)','Retur*Dist. route*Seizures (Q)',
                        'Dist. route*Seizures (N)','Retur*Dist. route*Seizures (N)')

stargazer(homi_q_ruta, homi_n_ruta, homi_q_ruta2, homi_n_ruta2,
          header=F, type= 'text',
          dep.var.labels.include = F,
          label = 'q',
          title = 'Effect of drugs scarcity on property crimes (ratio x 100.000)',
          column.labels = c('UNODC', 'Insight crime'),
          column.separate = c(2, 2),
          covariate.labels = labels_dependientes, 
          table.placement = 'H', 
          df = FALSE,
          digits = 4, 
          omit.stat = c('f', 'ser', 'rsq', 'adj.rsq'),
          add.lines = list(c('Time frame', '2016-2020', '2016-2020', '2016-2020', '2016-2020'),
                           c('F.E. Municipality', 'Yes', 'Yes', 'Yes', 'Yes'),
                           c('F.E. Year-Month', 'Yes', 'Yes', 'Yes', 'Yes')),
          notes.align = "l", notes.append = T,
          out = '6_informe/input/tables/reghdfe_2routes_ratio_property.tex')

#=====================#
# Number of violencia #
#=====================#

# import data
db = import("4_estimations/output/data_mpo_year-month_2016_2020.rds") %>% subset(d_2016_2020==1) 

# models
modelo_q_ruta = '~ retor_m + q_10_ruta + q_10_ruta_ret | mpio_code + year_month | 0 | mpio_code'
modelo_n_ruta = '~ retor_m + n_10_ruta + n_10_ruta_ret | mpio_code + year_month | 0 | mpio_code'

# results
homi_q_ruta = felm(as.formula(paste0("violencia",modelo_q_ruta)) , data=db , subset=d_2016_2020==1) 
homi_n_ruta = felm(as.formula(paste0("violencia",modelo_n_ruta)) , data=db , subset=d_2016_2020==1) 

db = db %>% 
        select(d_2016_2020,violencia,retor_m,mpio_code,year_month,q_10_ruta2,n_10_ruta2,q_10_ruta2_ret,n_10_ruta2_ret) %>% 
        rename(q_10_ruta=q_10_ruta2 , n_10_ruta=n_10_ruta2 , q_10_ruta_ret=q_10_ruta2_ret , n_10_ruta_ret=n_10_ruta2_ret)

homi_q_ruta2 = felm(as.formula(paste0("violencia",modelo_q_ruta)) , data=db , subset=d_2016_2020==1) 
homi_n_ruta2 = felm(as.formula(paste0("violencia",modelo_n_ruta)) , data=db , subset=d_2016_2020==1) 

# Tabla prinicpal
labels_dependientes = c('Returnees',
                        'Dist. route*Seizures (Q)','Retur*Dist. route*Seizures (Q)',
                        'Dist. route*Seizures (N)','Retur*Dist. route*Seizures (N)')

stargazer(homi_q_ruta, homi_n_ruta, homi_q_ruta2, homi_n_ruta2,
          header=F, type= 'text',
          dep.var.labels.include = F,
          label = 'q',
          title = 'Effect of drugs scarcity on property crimes',
          column.labels = c('UNODC', 'Insight crime'),
          column.separate = c(2, 2),
          covariate.labels = labels_dependientes, 
          table.placement = 'H', 
          df = FALSE,
          digits = 4, 
          omit.stat = c('f', 'ser', 'rsq', 'adj.rsq'),
          add.lines = list(c('Time frame', '2016-2020', '2016-2020', '2016-2020', '2016-2020'),
                           c('F.E. Municipality', 'Yes', 'Yes', 'Yes', 'Yes'),
                           c('F.E. Year-Month', 'Yes', 'Yes', 'Yes', 'Yes')),
          notes.align = "l", notes.append = T,
          out = '6_informe/input/tables/reghdfe_2routes_abs_violents.tex')


#============================#
# violencia ratio by 100.000 #
#============================#

# clean environment
rm(list=ls())

# import data
db = import("4_estimations/output/data_mpo_year-month_2016_2020.rds") %>% subset(d_2016_2020==1) 

# models
modelo_q_ruta = '~ retor_m + q_10_ruta + q_10_ruta_ret | mpio_code + year_month | 0 | mpio_code'
modelo_n_ruta = '~ retor_m + n_10_ruta + n_10_ruta_ret | mpio_code + year_month | 0 | mpio_code'

# results
homi_q_ruta = felm(as.formula(paste0("ratio_propiedad",modelo_q_ruta)) , data=db , subset=d_2016_2020==1) 
homi_n_ruta = felm(as.formula(paste0("ratio_propiedad",modelo_n_ruta)) , data=db , subset=d_2016_2020==1) 

db = db %>% 
        select(d_2016_2020,ratio_propiedad,retor_m,mpio_code,year_month,q_10_ruta2,n_10_ruta2,q_10_ruta2_ret,n_10_ruta2_ret) %>% 
        rename(q_10_ruta=q_10_ruta2 , n_10_ruta=n_10_ruta2 , q_10_ruta_ret=q_10_ruta2_ret , n_10_ruta_ret=n_10_ruta2_ret)

homi_q_ruta2 = felm(as.formula(paste0("ratio_propiedad",modelo_q_ruta)) , data=db , subset=d_2016_2020==1) 
homi_n_ruta2 = felm(as.formula(paste0("ratio_propiedad",modelo_n_ruta)) , data=db , subset=d_2016_2020==1) 

# Tabla prinicpal
labels_dependientes = c('Returnees',
                        'Dist. route*Seizures (Q)','Retur*Dist. route*Seizures (Q)',
                        'Dist. route*Seizures (N)','Retur*Dist. route*Seizures (N)')

stargazer(homi_q_ruta, homi_n_ruta, homi_q_ruta2, homi_n_ruta2,
          header=F, type= 'text',
          dep.var.labels.include = F,
          label = 'q',
          title = 'Effect of drugs scarcity on property crimes (ratio x 100.000)',
          column.labels = c('UNODC', 'Insight crime'),
          column.separate = c(2, 2),
          covariate.labels = labels_dependientes, 
          table.placement = 'H', 
          df = FALSE,
          digits = 4, 
          omit.stat = c('f', 'ser', 'rsq', 'adj.rsq'),
          add.lines = list(c('Time frame', '2016-2020', '2016-2020', '2016-2020', '2016-2020'),
                           c('F.E. Municipality', 'Yes', 'Yes', 'Yes', 'Yes'),
                           c('F.E. Year-Month', 'Yes', 'Yes', 'Yes', 'Yes')),
          notes.align = "l", notes.append = T,
          out = '6_informe/input/tables/reghdfe_2routes_ratio_property.tex')

